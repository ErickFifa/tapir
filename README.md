# Temporal API Recommender (TAPIR)
To achieve any meaningful task of a certain
complexity, developers need to use APIs. Learning to use them
is both time consuming and cognitively demanding. We propose
to leverage a formal description of API usage as temporal
patterns to help developers make sense of the complexities
of working with APIs. To achieve this, we propose to deploy
recommender systems TAPIR  at various points of the development
process that make these patterns useful when most needed. 
--- 
TAPIR is still a work in progress 


# Installation requirements

###Google test 
Gtest can be downloaded [here](https://github.com/google/googletest) . gtest version 1.7.0 is recommended. 

###Boost 
Boost can be downloaded [here](https://www.boost.org/doc/libs/1_56_0/more/getting_started/unix-variants.html). Instructions for installation are available on their website 
###Spot 
Spot can be download [here](https://www.lrde.epita.fr/dload/spot/) . Texada is currently compatible with  a spot version of 1.2.4 - 1.2.6 


### Texada 
Texada source code can be available on this bitbucket repository with it's build instruction:
[Texada repository](https://bitbucket.org/bestchai/texada/src/default/) 

### Graphviz 
A visualisation software is required to convert output from TAPIR (dot files) into images. 
Graphviz can be downloaded [here](https://www.graphviz.org/download/).
### Grapacc 
Grapacc is an Eclipse plugin that produces code completion recommendations
based on API usage patterns. Information and download link can be found [here](http://home.engineering.iastate.edu/~anhnt/Research/GraPacc/?page=introduction).

# License
projects in the TAPIR repository are distributed under the 3-clause BSD license, which appears below.
Copyright (c) 2019, Université de Montréal

Redistribution and use in source and binary forms, 
with or without modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

    Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
 THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
   INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
     INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
      ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE