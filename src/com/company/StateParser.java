package com.company;

public class StateParser {
    static class State{
        String pre, post, op;
        State(String pre, String post, String op){
            this.pre = pre;
            this.post = post;
            this.op = op;
        }
        State(){
            this.pre = null;
            this.post = null;
            this.op = null;
        }

    }
    static void parseState(String s,State st){
     //  System.out.println("S= " + s);
        String[] split = s.split(" ");
      //  System.out.println("length= " + split.length);
        if (split.length==3){
            st.pre = split[0];
            st.op = split[1];
            st.post = split[2];
           // System.out.println("s1" + split[1]);
        } else{
            st.pre = s;

        }

    }
    static void processState(State st){

        if(st.op != null){

            if(st.post.contains("!")){

                //todo remove negative state of state
                st.post = null;
                st.op = null;
            }
            if(st.pre.contains("!")){
                //todo remove negative state of pre state
                st.pre = null;
                st.op = null;
            }
        }

    }
    static String getState(State st){
    String res = null;
    if(st.post == null){
        res = st.pre;
    }
    if(st.pre == null){
       res = st.post;
    }
   // res = st.pre;
    return res;
    }
}
