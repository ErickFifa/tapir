package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class Parser  {
    static class File{
        String path;
        String[] lines;
        Main.Graph g;
        File(String path, Main.Graph graph, int size){
            this.path = path;
            this.lines = new String[size];
            this.g = graph;
        }
    }
    static void ReadFile(File f){
        BufferedReader br = null;
        FileReader fr = null;
        int i = 0;
        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(f.path);
            br = new BufferedReader(fr);

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                //System.out.println(sCurrentLine);
                f.lines[i] = sCurrentLine;
                i++;
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();
                printFile(f);

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
    }
    static void printFile(File f){
        for (int i = 0; i<f.lines.length; i++){

            if(f.lines[i] != null){
             //   System.out.println(f.lines[i]);
            }
        }
    }
    static void processLine(File f){
        String line;
        for (int l = 0; l<f.lines.length; l++){
            //crawl the table
            //eachlines check if it is a state description or edge description
            line = f.lines[l];
            if(line != null){
                if(checkLine(line)){
                    parseLabel(line, f);
                }else{
                    //TODO if the line is an edge.
                    parseEdge(line, f);
                }
            }

        }
      //  Main.printDotGraph(f.g);
    }
    static boolean checkLine(String l){ //if true then it's a declaration else it's an edge.
       if (l.contains("label")){
           return true;
       }else {
           return false;
       }
    }
    protected static void sanitize(File f){
        ReadFile(f);
        for (int l = 0; l<f.lines.length; l++){
            if(f.lines[l] != null){
                f.lines[l] = f.lines[l].trim(); //Trim whitespace

                //remove space around the arrows
                if(f.lines[l].contains("->")){
                    int s =f.lines[l].indexOf("-");
                    int e = f.lines[l].indexOf(">");
                    String selected = f.lines[l].substring(s-1 , e+2);
                    f.lines[l] = f.lines[l].replace(selected, "->");
                }
                if(f.lines[l].contains("\\n")){ //remove nextline char
                    f.lines[l] = f.lines[l].replace("\\n","");

                }
                if( f.lines[l].contains("{Acc[1]")){ //remove 1-loop
                    int s =f.lines[l].indexOf("{");
                    int e = f.lines[l].indexOf("}");
                    String selected = f.lines[l].substring(s , e+1);
                    f.lines[l] = f.lines[l].replace(selected, "");
                   // f.lines[l] = null;
                }
            }

        }

    }
    static void parseLabel(String l, File f){
        String[] preproc = l.split(" ", 2);
       // System.out.println("prerpoc 0= " + preproc[0]);
        int startIndex = preproc[1].indexOf("[");
        int endIndex = preproc[1].indexOf("=");
        String replacement = "";
        String toBeReplaced = preproc[1].substring(startIndex , endIndex + 2);
        preproc[1] = preproc[1].replace(toBeReplaced,replacement);
        startIndex = preproc[1].indexOf("\"");
        endIndex = preproc[1].indexOf("]");
        toBeReplaced = preproc[1].substring(startIndex , endIndex +1);
        preproc[1] = preproc[1].replace(toBeReplaced,replacement); //preproc[1] contains the label value
        StateParser.State st = new StateParser.State();
        StateParser.parseState(preproc[1],st);
        StateParser.processState(st);
        parseEdge(preproc[0],f);

        if(preproc[0].contains("->")){
            //todo push the label
            String[] splited = l.split("->");
            parseLabel(splited[1], f);
           // System.out.println("splited[1] = "+ splited[1]);
            //Main.addLabel(f.g, Integer.parseInt(splited[1]) , StateParser.getState(st));
        }else{
            //System.out.println("preproc[0] = "+ preproc[0]);
            Main.addLabel(f.g, Integer.parseInt(preproc[0]) , StateParser.getState(st));
        }


       // parseEdge(splited[0],f);
      //  String[] secondSplited = splited[0].split("->");
      //  Main.addLabel(f.g,Integer.parseInt(secondSplited[1]),splited[1]);
    }
    static  void parseEdge(String l, File f){
        if(l.contains("->")){
            String[] splited = l.split("->");
            Main.addEdge(f.g,Integer.parseInt(splited[0]),Integer.parseInt(splited[1]));
        }

    }
    public static void main(String args[]) {
        String path ="./Trans.spot";
        Main.Graph g = new Main.Graph(200);
        File f = new File(path,g,20);
        sanitize(f);
        processLine(f);
        Main.printDotGraph(g);
      //  parseLabel("0 [label=\"!a | b\"]", null);

    }
}