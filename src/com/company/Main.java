package com.company;


import java.io.*;
import java.util.*;

import com.company.Parser;
public class Main {

        static class Graph
        {
            int V;
            LinkedList<Integer> adjListArray[];
            String[] lab;
            // constructor
            Graph(int V)
            {
                this.V = V;
                adjListArray = new LinkedList[V];
                lab = new String[V];
                for(int i = 0; i < V ; i++){
                    adjListArray[i] = new LinkedList<>();
                }
            }

            public String[] getLab() {
                return lab;
            }

            public LinkedList<Integer>[] getAdjListArray() {
                return adjListArray;
            }
            public  void copyGraph(Graph g){
                g.V = this.V;
                g.lab = this.lab;
                g.adjListArray = this.adjListArray;
            }
        }


        public static void addEdge(Graph graph, int src, int dest)
        {
            // Add an edge from src to dest.
            graph.adjListArray[src].add(dest);

        }
        public static void addLabel(Graph graph, int st, String name){
            if(graph.lab[st]!= null){
                //TODO append and remove blank
                if(graph.lab[st]!= ""){
                    graph.lab[st] += " & "+ name;
                }

            }else{
                graph.lab[st] = name;
            }

        }
        public static void getLabel(Graph graph){
            for(int i = 0 ; i < graph.getLab().length ; i++){
                if( graph.lab[i] != null){
                    if(graph.lab[i].equals("")){
                        System.out.println("Vide");
                    }
                    System.out.println( "i = "+ i + " ->  "+graph.lab[i]) ;
                }

            }
        }
        // representation of graph
        public static void printDotGraph(Graph graph)
        {
            System.out.println("digraph G{");
            for(int v = 0; v < graph.V; v++)
            {
               // System.out.println("Adjacency list of vertex "+ v);
                if(graph.lab[v] != null){
                    if(graph.lab[v].equals("")){
                        System.out.println(v + " [ label= \"" +graph.lab[v] + "\", style=invis ]");
                    }else{
                        System.out.println(v + " [ label= \"" +graph.lab[v] + "\" ]");
                    }

            }

                for(Integer pCrawl: graph.adjListArray[v]){
                    if(pCrawl == v){
                        break;
                    }
                    System.out.println(v+"->"+pCrawl);
                }

            }
            System.out.println("}");

        }
    static void printConfig(Graph graph,String warn, String ID, int V,boolean[] visit,List<Integer> ll) throws IOException{
        FileWriter fileWriter = new FileWriter("./config.conf",true);
        PrintWriter pw = new PrintWriter(fileWriter);
        String stacker = "";
        pw.println("Pattern: " + ID);
        pw.println("Size:");
        pw.println("Freq:10");
        pw.println("File:CorePatterns.java");

        int count = 0;
        for(int v = 0; v < graph.V; v++){ //count nodes value
            if(graph.lab[v] != null) {
                count++;
            }

        }
        pw.println("nodes:"+count);

        for(int v = 0; v < graph.V; v++) //print Label
        {
            if(graph.lab[v] != null){
                pw.println(v + ":" +graph.lab[v]);
            }

        }

        count =0;
        pw.println("grams:"+ count);
        printGram(graph,V,visit,ll,pw);
        for(int v = 0; v < graph.V; v++){
            for(Integer pCrawl: graph.adjListArray[v]){
                stacker += v+"->"+pCrawl+"\n";
                count++;
            }
        }


        pw.println("RepTokens:0");
        pw.println("RepSrc:1");
        pw.println(warn);
        pw.println("Edges:"+count); //write edges.
        pw.print(stacker);
        pw.close();


    }




    private static void findGram(Graph g, Integer u, Integer d,
                                   boolean[] isVisited,
                                   List<Integer> localPathList,String source, PrintWriter pw ) {

        // Mark the current node
        isVisited[u] = true;
        if (u.equals(d))
        {
           String stacker = source + ":";
//            String stacker = + ":";
            if(localPathList.isEmpty() ){
                pw.println(source);
                return ;
            }else{
                for(int i=0;i<localPathList.size();i++){
                   stacker += Integer.toString(localPathList.get(i)) +":" ;
                }
                stacker = stacker.substring(0,stacker.length() -1) ;
                pw.println(stacker);
                // if match found then no need to traverse more till depth
                isVisited[u]= false;
                return;
            }

        }

        // Recur for all the vertices
        // adjacent to current vertex
        for (Integer i : g.adjListArray[u])
        {
            if (!isVisited[i])
            {
                // store current node
                // in path[]
                localPathList.add(i);
                findGram(g,i, d, isVisited, localPathList,source,pw);

                // remove current node
                // in path[]
                localPathList.remove(i);
            }
        }

        // Mark the current node
        isVisited[u] = false;
    }
    private static void printGram(Graph g, int v,boolean[] isVisited, List<Integer> ll,  PrintWriter pw ){
        for(int s = 0; s<v ; s++){
            //System.out.print(s+":");
            for (int d =0; d<v ; d++){
                findGram(g,s,d,isVisited,ll,Integer.toString(s),pw);
            }
            //System.out.println("---------");
        }
    }
    private static void execSpot(String formula){
           String invokeSpot = "ltl2tgba -B ";
           String fileName = "> Trans.spot";
           String com = invokeSpot + formula+fileName;
           //generating the script file
        //FileUtils.writeStringToFile(new File("spot.sh"), com);
            try (PrintWriter out = new PrintWriter("spot.sh")) {
            out.println(com);
             }catch(Exception e){}
           System.out.println(com);
            try{
                Process p = Runtime.getRuntime().exec("sh spot.sh"); //TODO change to direct command
               // p.getErrorStream();
                p.waitFor();
                System.out.println ("exit: " + p.exitValue());
                p.destroy();
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    private static void render(String path, int i){
        String invoke = "dot -Tpng ";
        String com = invoke + path + " > out"+ i+ ".png";
        //generating the script file
        try (PrintWriter out = new PrintWriter("dot.sh")) {
            out.println(com);
        }catch(Exception e){}
        System.out.println(com);
        try{
            Process p = Runtime.getRuntime().exec("sh dot.sh"); //TODO change to direct command
            // p.getErrorStream();
            p.waitFor();
            System.out.println ("exit: " + p.exitValue());
           // p.destroy();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private static void graphProcessor( Graph g){
            Graph pGraph = new Graph(100);
             String[] s;
            //processing the node
        for(int i = 0 ; i < g.getLab().length ; i++){
            if( g.lab[i] != null){
                if(g.lab[i].equals("")){
                    g.lab[i] = null;
                    g.adjListArray[i].remove();
                    System.out.println("an empty was found");
                }else if(isnum(g.lab[i])){
                    g.lab[i] = null;
                    System.out.println(g.adjListArray.length);
                    while(!g.adjListArray[i].isEmpty()){
                        g.adjListArray[i].remove();
                    }
                    }else if(g.lab[i].contains("&")){  //check if contains multiple state
                    s = g.lab[i].split("&");
                    g.lab[i] = "";
                    //foreach s delete if numerical . if not then delete
                    for (int c = 0; c < s.length ; c++){
                        if(isnum(s[c])){

                            s[c] = "";
                        }else{
                            s[c] = s[c].replace(" ","");
                        }
                        //check if states are positives. if not then delete
                        if(s[c].contains("!")){
                            s[c] = "";
                        }
                    }

                    for(int c = 0; c < s.length ; c++){
                        //remove duplicated label
                        System.out.println("Show me: "+s[c]+ " c= " + c);
                        if(s.length>0 && c+1 < s.length){
                            if(s[0] == s[c]){
                                System.out.println("Skiiped");
                            }
                        }
                        g.lab[i] += s[c];



                    }
//                    if(g.lab[i].matches(".*([ \\t]).*")){
//                        System.out.println("I m in");
//                    }
                }


            }

        }
        //Split the double in two different edge
        }
    static  boolean isnum(String s){
        try {
            double d = Double.parseDouble(s);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
    public static void outputDotGraph(Graph graph) throws  IOException{
            System.out.println("printing dot File....");
        FileWriter fwritter = new FileWriter("./output.dot");
        PrintWriter ww = new PrintWriter(fwritter);
       ww.println("digraph G{");
        for(int v = 0; v < graph.V; v++)
        {
            if(graph.lab[v] != null){
                if(graph.lab[v].equals("")){
                    ww.println(v + " [ label= \"" +graph.lab[v] + "\", style=invis ]");
                }else{
                    ww.println(v + " [ label= \"" +graph.lab[v] + "\"  shape=box ]");
                }

            }

            for(Integer pCrawl: graph.adjListArray[v]){
                if(pCrawl == v){
                    break;
                }
                ww.println(v+"->"+pCrawl);
            }

        }
        ww.println("}");
        ww.close();
    }
    protected static void parseLtlSet(String path){
        int v = 100;
        boolean[] visit = new boolean[v];
        List<Integer> ll = new ArrayList<Integer>();
        Graph graph = new Graph(v);
        BufferedReader br = null;
        FileReader fr = null;
        Map<String, String> map = new HashMap<String, String>();
        String value;
        int i=0;
        try {
            fr = new FileReader(path);
            br = new BufferedReader(fr);
            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                execSpot(sCurrentLine);
                Parser.File f = new Parser.File("Trans.spot",graph,20);
                Parser.sanitize(f);
                Parser.processLine(f);
                graphProcessor(graph);
                printDotGraph(graph);

                try {
                    outputDotGraph(graph);


                }catch (IOException e ){
                    e.printStackTrace();
                }
                render("./output.dot", i);
                i++;

            }

        }catch ( IOException ex){
            ex.printStackTrace();
        }

    }
    private static  void buildConfig(String path)  throws IOException {
//       ------pre-requis pour le graph
        int v = 100;
        boolean[] visit = new boolean[v];
        List<Integer> ll = new ArrayList<Integer>();
        Graph graph = new Graph(v);
//        ---------
        File fichier = new File(path);
        String appender="";
        try {
            Scanner scanner = new Scanner(fichier);
            while (scanner.hasNextLine()) {
                appender = appender + "#" + scanner.nextLine();
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //TODO add a error catcher if i < 1
        String[] item = appender.split("#");
        //flatten after 2
        appender = "";
        for (int j = 3; j < item.length ; j++) {
            appender += " "+ item[j];
        }
        System.out.println("this is appender " + appender);
//      --- building the graph ------------
        execSpot(item[1]);
        Parser.File f = new Parser.File("Trans.spot",graph,v);
        Parser.sanitize(f);
        Parser.processLine(f);
        graphProcessor(graph);
//       -----------------------------
        printConfig(graph,appender,item[1],v,visit,ll);

    }
    private static void execTexada(String formula, String path){
        String invokeTexada = "./texada -m -f "
                +  formula
                + " --log-file " +path;
        try (PrintWriter out = new PrintWriter("texada.sh")) {
            out.println(invokeTexada);
        }catch(Exception e){}
        try{
            Process p = Runtime.getRuntime().exec("sh texada.sh"); //TODO change to direct command
            // p.getErrorStream();
            p.waitFor();
            BufferedReader lineReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            lineReader.lines().forEach(System.out::println);
            System.out.println ("exit: " + p.exitValue());
            p.destroy();
        }catch(Exception e){
            e.printStackTrace();
        }


    }
    // Driver program to test above functions
        public static void main(String args[]) throws IOException
        {
            int v = 100;
            boolean[] visit = new boolean[v];
            List<Integer> ll = new ArrayList<Integer>();
            Graph graph = new Graph(v);
            String query = "\"open-> XF ( request-> XF close)\"";
           /* addEdge(graph, 0, 1);
            addEdge(graph, 0, 2);
            addEdge(graph, 1, 2);
            addLabel(graph,0,"a");
            addLabel(graph,1,"b");
            addLabel(graph,2,"c");*/
            //execSpot(" \"(a->X(b->XFc))\"");
            execSpot(query);
            Parser.File f = new Parser.File("Trans.spot",graph,20);
            Parser.sanitize(f);
            Parser.processLine(f);
            graphProcessor(graph);
            getLabel(graph);
            try {
                outputDotGraph(graph);
            }catch (IOException e ){
                e.printStackTrace();
            }

            printDotGraph(graph);
////            printConfig(graph,"A warning test",20,visit,ll);
//            //getLabel(graph);
            render("./output.dot",1);
            System.out.println("---------");
           /* try {
                System.out.println("Printing config files");
                printConfig(graph,"Warning",v,visit,ll);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
//            parseLtlSet("ltlSet.tapir");
            //printGram(graph,v,visit,ll);
            //findGram(graph,0,2,visit,ll,"0");
        }

}
