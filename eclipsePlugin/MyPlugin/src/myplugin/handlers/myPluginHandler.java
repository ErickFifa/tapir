package myplugin.handlers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
//import org.eclipse.jdt.core.IClasspathEntry;
//import org.eclipse.jdt.core.ICompilationUnit;
//import org.eclipse.jdt.core.IJavaProject;
//import org.eclipse.jdt.core.IPackageFragment;
//import org.eclipse.jdt.core.IPackageFragmentRoot;
//import org.eclipse.jdt.core.IType;
//import org.eclipse.jdt.core.JavaCore;
//import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.*;
import org.eclipse.jdt.core.*;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.ForStatement;
import org.eclipse.ui.*;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.internal.corext.callhierarchy.CallHierarchy;
import org.eclipse.jdt.internal.corext.callhierarchy.MethodWrapper;

public class myPluginHandler extends AbstractHandler {
	public static void printInPopUpWindow(ExecutionEvent event, String msg) throws ExecutionException {

		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		MessageDialog.openInformation(window.getShell(), "TAPIR", msg);
	}

	public HashSet<IMethod> getCallersOf(IMethod m) {

		CallHierarchy callHierarchy = CallHierarchy.getDefault();

		IMember[] members = { m };

		MethodWrapper[] methodWrappers = callHierarchy.getCallerRoots(members);
		HashSet<IMethod> callers = new HashSet<IMethod>();
		for (MethodWrapper mw : methodWrappers) {
			MethodWrapper[] mw2 = mw.getCalls(new NullProgressMonitor());
			HashSet<IMethod> temp = getIMethods(mw2);
			callers.addAll(temp);
		}

		return callers;
	}

	HashSet<IMethod> getIMethods(MethodWrapper[] methodWrappers) {
		HashSet<IMethod> c = new HashSet<IMethod>();
		for (MethodWrapper m : methodWrappers) {
			IMethod im = getIMethodFromMethodWrapper(m);
			if (im != null) {
				c.add(im);
			}
		}
		return c;
	}

	IMethod getIMethodFromMethodWrapper(MethodWrapper m) {
		try {
			IMember im = m.getMember();
			if (im.getElementType() == IJavaElement.METHOD) {
				return (IMethod) m.getMember();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void insideMethodCallList(IType[] allTypes) throws JavaModelException {
		for (IType type : allTypes) {
			IMethod[] methods = type.getMethods();

			for (IMethod method : methods) {

				System.out.println("--Method name: " + method.getElementName());
//				System.out.println("Signature: " + method.getSignature());
//				System.out.println("Return Type: " + method.getReturnType());
				parseMethodSource(method.getSource());
				// System.out.println("callers of this method: "+getCallersOf(method));
//				System.out.println("source: " + method.getSource());
//						===== show root of the method
//						System.out.println("to string: "+ method.toString());
//						System.out.println("new: "+ method.getPath().toString());
				// Parse method signature to flag a main method...
//						method.getSignature()=="([QString;)V"
//				if ("([QString;)V".equals(method.getSignature())) {
//					mainFlag = true;
//				}
			}
		}

	}


	public static void parseMethodSource(String tempMiddles) {
		String[] tempMiddle = tempMiddles.split("\n");
		String sourceStart = "public class Shell {";
		// add a fake class A as a shell, to meet the requirement of ASTParser
		String sourceMiddle = "";
		// temp middle is probbly an array of string separated by /n
		for (String s : tempMiddle) {
			System.out.println("S =: " + s);
			s = s.trim(); // removes extra spaces
			if (s.trim().length() > 0 && !s.startsWith("---") && !s.startsWith("/") && !s.startsWith("*"))
				sourceMiddle += s.trim() + "\n";
		}
		String sourceEnd = "}";
		String source = sourceStart + sourceMiddle + sourceEnd;

		ASTParser parser = ASTParser.newParser(AST.JLS11);
		parser.setSource(source.toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

		cu.accept(new ASTVisitor() {

			Set names = new HashSet();

			public boolean visit(VariableDeclarationFragment node) {
				SimpleName name = node.getName();
				this.names.add(name.getIdentifier());
				
//				System.out.println("Declaration of '" + name + "' at line" + cu.getLineNumber(name.getStartPosition()));
				return false; // do not continue to avoid usage info
			}

			public boolean visit(SimpleName node) {
				if (this.names.contains(node.getIdentifier())) {
//					System.out.println("Usage of '" + node + "' at line " + cu.getLineNumber(node.getStartPosition()));
				}
				return true;
			}
			
			public boolean visit(MethodInvocation node) {
				//TODO get lower level method invocation into the AST.. Or programmatically resolve low level method binding
//				IMethodBinding methodBinding = node.resolveMethodBinding();
				System.out.println("Methode invoked: "+ node.getName() +" At line: "+ cu.getLineNumber(node.getStartPosition()));
				return true;
			}

		});

		/*
		 * IfStatement ForStatement WhileStatement DoStatement TryStatement
		 * SwitchStatement SynchronizedStatement
		 */
	}

//-------- Visit statement for CompilationUnit ( per type) 
	public boolean visit(ForStatement node) {

		System.out.println("ForStatement -- content:" + node.toString());

		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(node.getStartPosition());
		al.add(node.getLength());
		System.out.println("al content: " + al);
//		statements.add(al);
		return false;
	}

	public boolean visit(IfStatement node) {

		System.out.println("IfStatement -- content:" + node.toString());

		ArrayList<Integer> al = new ArrayList<Integer>();
		al.add(node.getStartPosition());
		al.add(node.getLength());
		System.out.println("al content: " + al);
//				statements.add(al);
		return false;
	}

//	------------------------
	public static void getMethodListInClassfile() { // output all method inside each class for a project
		int totalMethod = 0;
		boolean mainFlag = false;
		try {
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			System.out.println("root" + root.getLocation().toOSString());

			IProject[] projects = root.getProjects();

			// process each project
			for (IProject project : projects) {

				System.out.println("project name: " + project.getName());

				if (project.isNatureEnabled("org.eclipse.jdt.core.javanature")) {
					IJavaProject javaProject = JavaCore.create(project);
					IPackageFragment[] packages = javaProject.getPackageFragments();

					// process each package
					for (IPackageFragment aPackage : packages) {

						// We will only look at the package from the source folder
						// K_BINARY would include also included JARS, e.g. rt.jar
						// only process the JAR files

						if (aPackage.getKind() == IPackageFragmentRoot.K_SOURCE) {

							for (ICompilationUnit unit : aPackage.getCompilationUnits()) {

								System.out.println("--class name: " + unit.getElementName());

								IType[] allTypes = unit.getAllTypes();
								insideMethodCallList(allTypes);

//									Separation des class
								System.out.print("=============================================== \n");
							}
						}
					}

				}

			}
			System.out.println("total number of methods: " + totalMethod);
			System.out.print("Main found? -> " + mainFlag);
		} catch (Exception e) {

		}
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// Get the root of the workspace
//				IWorkspace workspace = ResourcesPlugin.getWorkspace();
//				IWorkspaceRoot root = workspace.getRoot();
//				// Get all projects in the workspace
//				IProject[] projects = root.getProjects();
//				// Loop over all projects
//				for (IProject project : projects) {
//					System.out.println(project.getName());
//				}

//		--------------------Get Method list in all class file ------------------
		getMethodListInClassfile();
// 		---------------------Test of parsing from method source 

		return null;
	}
}
